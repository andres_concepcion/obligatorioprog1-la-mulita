var preguntas = new Array ();
var respuestas = new Array ();
var jugadores = new Array ();
var numPregunta = darNumero();
var celdas = new Array();	
var colores = new Array();
colores = ["#009900","#FF0000","#FF3300","#B82E8A","#FFCC00"];	

preguntas[0] = new Array("Algunos animales, como los peces, tienen branquias. Para que las utilizan?",1,1);
preguntas[1] = new Array("Cuanto vale el resto de una divicion exacta?",3,1);
preguntas[2] = new Array("Indica cual de los siguientes astros tiene luz propia",2,1);
preguntas[3] = new Array("En el ciclo del agua, como llega el vapor de agua de la atmosfera a la superficie terrestre?",4,1);
preguntas[4] = new Array("Que nombre recibe el proceso por el cual los alimentos son transformados en sustancias mas sencillas en el estomago?",3,1);
preguntas[5] = new Array("Donde se encuentra la causa de las mareas, que provoca que el nivel de las aguas del mar suban o bajen?",1,1);
preguntas[6] = new Array("Que nombre tiene la ciencia que estudia el pasado del hombre?",3,1);
preguntas[7] = new Array("Indica cual de los siguientes tipos de energia es renovable",4,1);
preguntas[8] = new Array("La capa de ozono es una zona de la atmosfera que cumple la funcion basica para los seres vivos del planeta. De cual se trata?",1,1);
preguntas[9] = new Array("Como se llama la sustancia que en las calulas vegetales permite absorber la luz solar para obtener energia?",1,1);
preguntas[10] = new Array("Cual  de las siguientes denominaciones no hace referencia a una masa de aire a gran velocidad y con efectos catastroficos?",2,1);
preguntas[11] = new Array("Que nombre reciben los pequeños astros de roca y hielo que en su orbita van dejando tras de si una enorme cola?",2,1);
preguntas[12] = new Array("Como se denomina a los objetos que dejan pasar la luz pero no permiten ver con nitidez los objetos",1,1);
preguntas[13] = new Array("Que nombre reciben los extremos de un iman?",3,1);
preguntas[14] = new Array("Con que sentido se relacionan las papilas?",3,1);
preguntas[15] = new Array("A partir de que otro tipo de energia se genera electricidad en las centrales eolicas?",4,1);
preguntas[16] = new Array("Los solidos pasan a estado liquido a una temperatura concreta llamada temperatura de ...?",3,1);
preguntas[17] = new Array("Si existe un objeto con volumen constante pero forma variable, en que estado se encuentra?",1,1);
preguntas[18] = new Array("Que aparato de los animales es el encargado de distribuir todos los nutrientes necesarios por las celulas del ser?",4,1);
preguntas[19] = new Array("Que nombre recibe la energia que se produce por existencia de una diferencia de potencial entre dos puntos?",2,1);

respuestas[0] = new Array("Para respirar debajo del agua","Para reproducirse","Para nadar","Para respirar");
respuestas[1] = new Array("Uno","Lo mismo que el divisor","Cero","Lo mismo que el cociente");
respuestas[2] = new Array("Satelite","Estrella","Planeta","Todos ellos");
respuestas[3] = new Array("Por las flitraciones","Por el viento","Por el calor del sol","Por las precipitaciones");
respuestas[4] = new Array("Espiracion","Deglucion","Digestion","Nutricion");
respuestas[5] = new Array("La fuerza de atraccion de la Luna y el Sol","El fuerte viento","Los terremotos en el fonde del mar","El cambio brusco de temperatura");
respuestas[6] = new Array("Arqueologia","Geografia","Historia","Fisica");
respuestas[7] = new Array("Carbon","Petroleo","Gas natural","Sol");
respuestas[8] = new Array("Filtra los rayos perjudiciales del sol","Impide la llegada de meteoritos a la Tierra","Provoca las lluvias y genera los vientos","Retiene el calor terrestre");
respuestas[9] = new Array("Clorofila","Almidon","Cloroplasto","Savia");
respuestas[10] = new Array("Huracan","Tsunami","Tifon","Ciclon");
respuestas[11] = new Array("Asteroides","Cometa","Satelites","Meteoritos");
respuestas[12] = new Array("Translucido","Refractante","Opaco","Transparente");
respuestas[13] = new Array("Bordes","Cabos","Polos","Bornes");
respuestas[14] = new Array("Oido","Vista","Gusto","Olfato");
respuestas[15] = new Array("Potencial","Gravitacional","Termica","Cinetica");
respuestas[16] = new Array("Evaporacion","Condensacion","Fusion","Ebullicion");
respuestas[17] = new Array("Liquido","Plasma","Gaseoso","Solido");
respuestas[18] = new Array("Aparato digestivo","Aparato respiratorio","Aparato excretor","Aparato circulatorio");
respuestas[19] = new Array("Potencial","Electrica","Mecanica","Radiante");

function tirarDado()
{
    var dado = Math.floor(Math.random() * 6 + 1);
    document.getElementById("dado").innerHTML="<img src='"+dado+".png' />";
    return (dado);    
}

function darNumero(tope)
{
    return Math.floor(Math.random()*tope);	
}

function cargarPreg()
{
    numPregunta = darNumero(19);
    
    while(preguntas[numPregunta][3]==0)
    {
        numPregunta = darNumero(19);
    }    
    
    document.getElementById("pregunta").value = preguntas[numPregunta][0];
    document.getElementById("txtrespuesta1").value = respuestas[numPregunta][0];
    document.getElementById("txtrespuesta2").value = respuestas[numPregunta][1];
    document.getElementById("txtrespuesta3").value = respuestas[numPregunta][2];
    document.getElementById("txtrespuesta4").value = respuestas[numPregunta][3];
        
    preguntas[numPregunta][3]=0;
    return (preguntas[numPregunta][3]);
}

function cargar()
{
    var tope = 5;
    for(var i=0; i<20; i++)
    {
        if((i+1)==1)
            continue;
        celdas[i] = darNumero(tope);
        while(celdas[i] == celdas[i-1])
            celdas[i] = darNumero(tope);
        switch(celdas[i])
        {
            case 0: //Madriguera
                document.getElementById("celda"+(i+1)).innerHTML = "Madriguera";
                break;
            case 1: //Pradera
                document.getElementById("celda"+(i+1)).innerHTML = "Pradera";
                break;
            case 2: //Trampa
                document.getElementById("celda"+(i+1)).innerHTML = "Trampa";
                break;
            case 3: //Insecto
               document.getElementById("celda"+(i+1)).innerHTML = "Insecto";
                break;
            case 4: //Arroyo
                document.getElementById("celda"+(i+1)).innerHTML = "Arroyo";
                break;
        }
        
        var valor = celdas[i];
        document.getElementById("celda"+(i+1)).style.backgroundColor = colores[valor];
    }
}

function pregunta()
{
    var correcto;
    var respJugador = parseInt(prompt("Ingrese la respuesta",""));
    respCorrecta = preguntas[numPregunta][1];
    if(respCorrecta == respJugador)
    {
        correcto = 1;
        alert("Respuesta correcta");
    }
    else
    {
        correcto = 0;
        alert("Respuesta incorrecta");
    }
    return(correcto);
}

function jugador()
{
    jugador1 = new Object();
    jugador2 = new Object();
    
    jugador1.nombre = document.getElementById("jugador1");
    jugador1.posicion = 1;
    jugador1.cantGanadas = 0;
    
    jugador2.nombre = document.getElementById("jugador2");
    jugador2.posicion = 1;
    jugador2.cantGanadas = 0;
    
    var control = 1;

    for(var i = 0; i<=jugadores.length; i++)
    {
        if(jugador1 != jugadores[i])
            control = 0;
        else
        {
            control = 1;
            break;
        } 
    }

    if(control == 0)
        jugadores.push(jugador1);

    for(var j = 0; j<=jugadores.length; j++)
    {
        if(jugador2 != jugadores[j])
            control = 0;
        else
        {
            control = 1;
            break;
        }
    }

    if(control == 0)
        jugadores.push(jugador2);
}

function moverFicha(jug,cant)
{
    if(jug == 1)
    {
        var posAnt = parseInt(jugador1.posicion);        
        document.getElementById("celda"+ posAnt).innerHTML = "";
        var posSig = posAnt + cant;
        if(posSig > 20)
        {
            document.getElementById("celda1").innerHTML = "<img src='peon_1.png' />";
            jugador1.posicion = posSig;
        }
        else
        {
            document.getElementById("celda"+ posSig).innerHTML = "<img src='peon_1.png' />";
            jugador1.posicion = posSig;
        }
        return(jugador1.posicion); 
    }
    else if(jug == 2)
    {
        var posAnt = parseInt(jugador2.posicion);        
        document.getElementById("celda"+ posAnt).innerHTML = "";
        var posSig = posAnt + cant;
        if(posSig > 20)
        {
            document.getElementById("celda1").innerHTML = "<img src='peon_2.png' />";
            jugador2.posicion = posSig;
        }
        else
        {
            document.getElementById("celda"+ posSig).innerHTML = "<img src='peon_2.png' />";
            jugador2.posicion = posSig;
        }
        return(jugador2.posicion); 
    }
      
}

function iniciarJuego()
{
    var child=document.getElementById("iniciarJuego");
    child.parentNode.removeChild(child);
    jugador();
    cargar();
    document.getElementById("celda1").innerHTML = "<img src = 'peon_1.png' />";
    var jugAct = 1;
    var jug = jugador1; 
    
    do
    {
        alert("Jugador " + jugAct + " tire el dado");
        var dado = tirarDado();
        moverFicha(jugAct, dado);
        var pos = jug.posicion;
        var casilla = parseInt(celdas[pos-1]);
        switch(casilla)
        {
            case 0: //Madriguera
                alert("Madriguera: \nLa mulita cae en la madriguera y descansa. Pierdes un turno");
                break;
            case 1: //Pradera
                alert("Pradera: \nDebes contestar una pregunta. Si respondes correctamente te quedas en el lugar si no retrocedes dos casilleros.");
                cargarPreg();
                var resp = pregunta();
                if(resp == 0)
                {
                    moverFicha(jugAct,(-2));
                    break;
                }
                else
                    break;
            case 2: //Trampa
                alert("Trampa: \nDebes contestar una pregunta. Si respondes correctamente avanzas dos casilleros si no retrocedes dos casilleros.");
                cargarPreg();
                var resp = pregunta();
                if(resp == 1)
                {
                    moverFicha(jugAct,2);
                    break;
                }
                else if(resp == 0)
                {    
                    moverFicha(jugAct,(-2));
                    break;
                }
            case 3: //Insecto
                alert("Insecto: \nDebes contestar una pregunta. Si respondes correctamente avanzas dos casilleros si no te quedas en el lugar.");
                cargarPreg();
                var resp = pregunta();
                if(resp == 1)
                {
                    moverFicha(jugAct,2);
                    break;
                }
                else
                    break;
            case 4: //Arroyo
                alert("Arroyo: \nLa mulita debera cruzar el arroyo y avanza automaticamente al proximo casillero.");
                moverFicha(jugAct,1);
                break;
           default:
               break;
        }
        
        if(jug.posicion >= 21)
            break;
        
        if(jugAct == 1)
        {
            jug = jugador2;
            jugAct = 2;
            
        }
        else if(jugAct == 2)
        {
            jug = jugador1;
            jugAct = 1;
        }
        
    }while(jugador1.posicion < 21 || jugador2.posicion < 21)
        
    jug.cantGanadas++;
    
    alert("¡El ganador es el jugador " + jugAct + "!");
}