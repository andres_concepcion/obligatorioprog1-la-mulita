La Mulita es un juego de mesa en el cual dos jugadores viajan a través del tablero y dependiendo en que casillero caigan depende el tipo de consecuencia que tendrá al responder la pregunta de múltiple opción pudiendo ser avanzar casilleros o retrocederlos.

#Módulo de carga de preguntas y respuestas.

Para las preguntas se utiliza un sistema de múltiple opción. Cada pregunta tiene 4 respuestas asociadas, de las cuales solamente una será correcta.
Las preguntas y las respuestas están almacenadas en dos variables de tipo array bidimensionales. Cada jugador se identifica con un color

#Inicio de la partida.

Se presenta un panel donde se ingresará el alias de los dos jugadores que formarán parte de la partida. Cada jugador ingresa su alias. El alias es único. Si no existe, automáticamente queda guardado.

Cuando ambos jugadores se identifiquen se le asigna un turno a cada uno(1 o 2) y comienza el juego.

Para cada casillero que contenga preguntas(salvo la madriguera), se asignarán al azar las preguntas del nivel que corresponda y sus respuestas. Las preguntas y respuestas no serán visibles para los jugadores hasta que caigan en el casillero.

Al inicio, ambos jugadores están en la salida.

#Continuación del juego.

El jugador que tiene el turno "tira el dado" para ver en que casillero cae. Una vez que cae en el casillero deberá realizar la acción que corresponda(contestar pregunta, perder un turno, avanzar automáticamente, según la regla del casillero). Cuando se trate de una pregunta, esta se elegirá aleatoriamente, y se deberá verificar que no sea una pregunta que haya salido previamente durante la sesión de juego (es decir, que no se haya preguntado desde que se cargó el juego en el navegador, como si se hubiera "quitado" la pregunta del mazo de preguntas posibles).

En un panel se mostrará la información del turno (el nombre del jugador con el turno, en qué tipo de casillero caerá, cuál es la regla del casillero, si responde correctamente o no, en caso de que la respuesta sea incorrecta cuál es la correcta, y toda información que resulte conveniente para verificar que está funcionando bien).

Cuando un jugador cae en un casillero, se deberá diferenciar si venía de tirar el dado cuando obtuvo el turno, o si cayó en él porque avanzó o retrocedió según la regla que contenía el casillero en el que había caído previamente. En el segundo caso, permanece en el casillero independientemente de la regla que contenga.

Cuando finalice, el turno pasa al otro jugador.
 
#Finalización del juego.

Cuando uno de los jugadores llega a la meta o la sobrepasa, el juego termina. Se muestra quien fue el ganador, y se despliega el ranking de jugadores (la lista de todos los jugadores, ordenados de mayor a menor según cantidad de partidas ganadas)